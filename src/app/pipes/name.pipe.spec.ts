import { NamePipe } from './name.pipe';
import { NameModel } from '../models/name.model';

describe('NamePipe', () => {
  it('create an instance', () => {
    const pipe = new NamePipe();
    expect(pipe).toBeTruthy();
  });

  it('should return NULL if data is not provided', () => {
    expect(new NamePipe().transform({} as NameModel)).toBeNull();
  });

  it('should return NAME string when NAME object is given', () => {
    const mockName = {
      id: 121,
      firstName: 'xyz',
      lastName: 'YE',
    };

    expect(new NamePipe().transform(mockName)).toBe('Xyz YE');
  });
});
