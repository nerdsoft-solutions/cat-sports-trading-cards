import { Pipe, PipeTransform } from '@angular/core';
import { NameModel } from '../models/name.model';
import { CommonUtils } from '../utils/common-utils';

@Pipe({
  name: 'name',
})
export class NamePipe implements PipeTransform {
  transform(value: NameModel): string | null {
    let nameString = '';
    if (CommonUtils.isObjectNullOrEmpty(value)) {
      return null;
    }

    nameString = nameString.concat(this.titleCase(value.firstName));

    nameString = nameString.concat(' ', this.titleCase(value.lastName));

    return nameString;
  }

  titleCase(val: string): string {
    return val.charAt(0).toUpperCase() + val.slice(1);
  }
}
