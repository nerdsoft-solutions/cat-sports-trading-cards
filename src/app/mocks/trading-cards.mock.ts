import { TradingCardsModel } from '../models/trading-cards.model';
import { Update } from '@ngrx/entity';

export const tradingCardsMock = [
  {
    playerNumber: 21,
    name: {
      firstName: 'FirstName',
      lastName: 'LastName',
    },
    teamName: 'TEAM',
    estimatedValue: 10,
  },
  {
    playerNumber: 22,
    name: {
      firstName: 'FirstName',
      lastName: 'LastName',
    },
    teamName: 'TEAM',
    estimatedValue: 21,
  },
] as Array<TradingCardsModel>;

export const tradingCardsLoadSuccess = {
  id: 21,
  changes: tradingCardsMock,
} as Update<TradingCardsModel>;
