export class CommonUtils {
  static isObjectNullOrEmpty<T = any>(obj: T) {
    if (!obj) {
      return true;
    }
    return obj && Object.keys(obj).length === 0;
  }
}
