import { CommonUtils } from './common-utils';

describe('CommonUtils', () => {
  it('should return true if the object is empty', () => {
    expect(CommonUtils.isObjectNullOrEmpty({})).toBeTruthy();
  });

  it('should return true if the object is null', () => {
    expect(CommonUtils.isObjectNullOrEmpty(null)).toBeTruthy();
  });

  it('should return false if the object is not empty', () => {
    expect(
      CommonUtils.isObjectNullOrEmpty({
        id: 21,
      })
    ).toBeFalsy();
  });
});
