import {
  animate,
  keyframes,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const FadeInOutAnimation = trigger('fadeInOut', [
  transition('void => *', [
    style({ opacity: 0 }),
    animate(
      1000,
      keyframes([
        style({ opacity: 0, offset: 0 }),
        style({ opacity: 0.3, offset: 0.3 }),
        style({ opacity: 0.8, offset: 0.6 }),
        style({ opacity: 1, offset: 1 }),
      ])
    ),
  ]),
  transition('* => void', [
    animate(
      1000,
      keyframes([
        style({ opacity: 1, offset: 0 }),
        style({ opacity: 0.8, offset: 0.3 }),
        style({ opacity: 0.3, offset: 0.6 }),
        style({ opacity: 0, offset: 1 }),
      ])
    ),
  ]),
]);
