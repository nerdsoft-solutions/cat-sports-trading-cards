export interface NameModel {
  firstName: string;
  lastName: string;
}
