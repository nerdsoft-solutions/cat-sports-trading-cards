export interface TradingCardsModel {
  playerNumber: number;
  name: {
    firstName: string;
    lastName: string;
  };
  teamName: string;
  estimatedValue?: number;
}
