import { Component, Input, ViewChild } from '@angular/core';
import { TradingCardsModel } from '../../models/trading-cards.model';

@Component({
  selector: 'app-trading-card',
  templateUrl: './trading-card.component.html',
  styleUrls: ['./trading-card.component.scss'],
})
export class TradingCardComponent {
  @Input() model: TradingCardsModel = {} as TradingCardsModel;
}
