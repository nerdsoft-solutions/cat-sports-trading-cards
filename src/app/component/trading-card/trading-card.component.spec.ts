import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingCardComponent } from './trading-card.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { NamePipe } from '../../pipes/name.pipe';

describe('TradingCardComponent', () => {
  let component: TradingCardComponent;
  let fixture: ComponentFixture<TradingCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [TradingCardComponent, NamePipe],
      imports: [RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
