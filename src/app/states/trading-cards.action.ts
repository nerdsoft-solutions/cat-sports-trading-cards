import { createAction, props } from '@ngrx/store';
import { TradingCardsModel } from '../models/trading-cards.model';
import { Update } from '@ngrx/entity';

export const addTradingCards = createAction(
  '[STC] Add Trading Card',
  props<{ data: TradingCardsModel }>()
);

export const loadTradingCardSuccess = createAction(
  '[STC] Load Trading Cards Success',
  props<{ data: Update<TradingCardsModel> }>()
);

export const loadTradingCardError = createAction(
  '[STC] Load Trading Cards Failed',
  props<{ error: Error }>()
);
