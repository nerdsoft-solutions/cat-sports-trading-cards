import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { TradingCardsModel } from '../models/trading-cards.model';
import { createReducer, on } from '@ngrx/store';
import * as TradingCardsActions from './trading-cards.action';

export const tradingCardsStateKey = 'trading-cards';

export interface TradingCardsState extends EntityState<TradingCardsModel> {
  loading: boolean;
  loaded: boolean;
  error: any;
}

export const adapter = createEntityAdapter<TradingCardsModel>({
  selectId: (model: TradingCardsModel) => model.playerNumber,
});

export const initialState: TradingCardsState = adapter.getInitialState({
  loaded: false,
  loading: false,
  error: null,
});

export const TradingCardsReducer = createReducer(
  initialState,

  on(TradingCardsActions.addTradingCards, (state, action) =>
    adapter.addOne(action.data, {
      ...state,
      loaded: false,
      loading: true,
      error: null,
    })
  ),

  on(TradingCardsActions.loadTradingCardSuccess, (state, action) =>
    adapter.updateOne(action.data, {
      ...state,
      loaded: true,
      loading: false,
      error: null,
    })
  ),

  on(TradingCardsActions.loadTradingCardError, (state, { error }) => {
    return {
      ...state,
      error,
    };
  })
);
