import * as TradingCardsActions from './trading-cards.action';
import {
  tradingCardsLoadSuccess,
  tradingCardsMock,
} from '../mocks/trading-cards.mock';

describe('Trading Cards Actions', () => {
  it('should return add cards action', () => {
    expect(
      TradingCardsActions.addTradingCards({ data: tradingCardsMock[0] }).type
    ).toBe('[STC] Add Trading Card');
  });

  it('should return update Cards Action', () => {
    expect(
      TradingCardsActions.loadTradingCardSuccess({
        data: tradingCardsLoadSuccess,
      }).type
    ).toBe('[STC] Load Trading Cards Success');
  });
});
