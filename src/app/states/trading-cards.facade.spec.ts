import { TradingCardsFacade } from './trading-cards.facade';
import { TestBed, tick } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import {
  TradingCardsReducer,
  tradingCardsStateKey,
} from './trading-cards.reducer';
import { tradingCardsMock } from '../mocks/trading-cards.mock';
import { take } from 'rxjs/operators';
import { TradingCardsModel } from '../models/trading-cards.model';

describe('TradingCardsFacade', () => {
  let facade: TradingCardsFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          [tradingCardsStateKey]: TradingCardsReducer,
        }),
      ],
      providers: [TradingCardsFacade],
    });

    facade = TestBed.inject(TradingCardsFacade);
  });

  it('should create', () => {
    expect(facade).toBeTruthy();
  });

  it('should return loading information when cards are getting loaded', () => {
    facade.addStcCard(tradingCardsMock[0]);

    facade.isLoading.pipe(take(1)).subscribe((value) => {
      expect(value).toBeFalsy();
    });
  });

  it('should return cards information when a card is added', () => {
    facade.addStcCard(tradingCardsMock[0]);

    facade.allTradingCards.pipe(take(1)).subscribe((value) => {
      expect(value).toStrictEqual([
        {
          playerNumber: 21,
          firstName: 'FirstName',
          lastName: 'LastName',
          teamName: 'TEAM',
        },
      ]);
    });
  });

  it('should return error when a card is unable to load', () => {
    facade.addStcCard({} as TradingCardsModel);

    facade.error.pipe(take(1)).subscribe((value) => {
      expect(value).toStrictEqual(new Error('unable to add the card'));
    });
  });

  it('should select all ids', () => {
    facade.addStcCard(tradingCardsMock[0]);
    facade
      .checkIfCardExist(21)
      .pipe(take(1))
      .subscribe((value) => {
        expect(value).toBeTruthy();
      });
  });
});
