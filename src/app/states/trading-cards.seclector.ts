import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  adapter,
  TradingCardsState,
  tradingCardsStateKey,
} from './trading-cards.reducer';

export const selectStcFeature =
  createFeatureSelector<TradingCardsState>(tradingCardsStateKey);

export const { selectIds, selectEntities, selectAll, selectTotal } =
  adapter.getSelectors();

export const selectCards = createSelector(selectStcFeature, selectAll);

export const getIds = createSelector(selectStcFeature, selectIds);

export const selectLoading = createSelector(
  selectStcFeature,
  (state: TradingCardsState) => state.loading
);
export const selectError = createSelector(
  selectStcFeature,
  (state: TradingCardsState) => state.error
);
