import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { TradingCardsState } from './trading-cards.reducer';
import { Observable } from 'rxjs';
import { TradingCardsModel } from '../models/trading-cards.model';
import {
  getIds,
  selectCards,
  selectError,
  selectLoading,
} from './trading-cards.seclector';
import * as TradingCardsActions from './trading-cards.action';
import { CommonUtils } from '../utils/common-utils';
import { map, take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class TradingCardsFacade {
  private readonly data: Observable<TradingCardsModel[]>;

  /** Observable to return if the store is loading or not*/
  isLoading: Observable<boolean>;

  /** Observable to return error if the card was unable to load*/
  error: Observable<Error>;

  constructor(private store: Store<TradingCardsState>) {
    this.data = this.store.select(selectCards);

    this.isLoading = this.store.select(selectLoading);
    this.error = this.store.select(selectError);
  }

  addStcCard(data: TradingCardsModel) {
    if (CommonUtils.isObjectNullOrEmpty(data)) {
      console.log('has error');
      this.store.dispatch(
        TradingCardsActions.loadTradingCardError({
          error: new Error('unable to add the card'),
        })
      );
    } else {
      this.store.dispatch(TradingCardsActions.addTradingCards({ data }));
      /*setTimeout(() => {
        this.store.dispatch(
          TradingCardsActions.loadTradingCardSuccess({
            data: {
              id: data.playerNumber,
              changes: {
                ...data,
              },
            },
          })
        );
      }, 600);*/
    }
  }

  get allTradingCards(): Observable<TradingCardsModel[]> {
    return this.data;
  }

  checkIfCardExist(id: number): Observable<boolean> {
    return this.store.select(getIds).pipe(
      map((ids) => {
        // @ts-ignore
        return !ids.includes(id);
      }),
      take(1)
    );
  }
}
