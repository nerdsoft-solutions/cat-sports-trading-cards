import * as TradingCardsActions from './trading-cards.action';
import { tradingCardsMock } from '../mocks/trading-cards.mock';
import {
  adapter,
  initialState,
  TradingCardsReducer,
} from './trading-cards.reducer';

describe('Trading Cards Reducer', () => {
  it('should be loading state', () => {
    const action = TradingCardsActions.addTradingCards({
      data: tradingCardsMock[0],
    });
    const result = TradingCardsReducer(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.loaded).toBeFalsy();
  });

  it('should be a success state', () => {
    const action = TradingCardsActions.loadTradingCardSuccess({
      data: {
        id: 21,
        changes: tradingCardsMock[0],
      },
    });
    const result = TradingCardsReducer(
      {
        ...adapter.getInitialState(),
        error: null,
        loading: true,
        loaded: false,
      },
      action
    );

    expect(result.loading).toBeFalsy();
    expect(result.loaded).toBeTruthy();
  });
});
