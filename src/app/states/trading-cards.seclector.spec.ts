import {
  selectCards,
  selectError,
  selectLoading,
  selectStcFeature,
} from './trading-cards.seclector';
import { tradingCardsMock } from '../mocks/trading-cards.mock';

describe('Trading Cards Selector', () => {
  it('should select the STC feature', () => {
    expect(
      selectStcFeature.projector({
        entities: null,
        loaded: false,
        loading: false,
        error: null,
      })
    ).toBeTruthy();
  });

  it('should select loading', () => {
    expect(
      selectLoading.projector({
        entities: null,
        loaded: false,
        loading: false,
        error: null,
      })
    ).toBe(false);
  });

  it('should select error', () => {
    expect(
      selectError.projector({
        entities: null,
        loaded: false,
        loading: false,
        error: new Error('test Error'),
      })
    ).toStrictEqual(new Error('test Error'));

    console.log(
      selectError.projector({
        entities: null,
        loaded: false,
        loading: false,
        error: new Error('test Error'),
      })
    );
  });
});
