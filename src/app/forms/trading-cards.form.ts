import { FormlyFieldConfig } from '@ngx-formly/core';
import { AbstractControl } from '@angular/forms';
import { TradingCardsFacade } from '../states/trading-cards.facade';

export const TRADING_CARDS_FORM = (
  facade: TradingCardsFacade
): FormlyFieldConfig[] => [
  {
    key: 'name',
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        key: 'firstName',
        type: 'input',
        className: 'flex-2',
        templateOptions: {
          label: 'FirstName',
          required: true,
          pattern: /^[A-Za-z]+$/,
        },
      },
      {
        key: 'lastName',
        type: 'input',
        className: 'flex-2',
        templateOptions: {
          label: 'LastName',
          required: true,
          pattern: /^[A-Za-z]+$/,
        },
      },
    ],
  },
  {
    key: 'teamName',
    type: 'input',
    templateOptions: {
      label: 'Team Name',
      required: true,
      pattern: /^(?![0-9]*$)[a-zA-Z0-9]+$/,
    },
    validation: {
      messages: {
        pattern: 'This field should contain both alphabets and numbers.',
      },
    },
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        key: 'playerNumber',
        type: 'input',
        className: 'flex-2',
        templateOptions: {
          label: 'Player Number',
          required: true,
          type: 'number',
        },
        asyncValidators: {
          playerNumberAlreadyExist: {
            expression: (control: AbstractControl) =>
              facade.checkIfCardExist(control.value).toPromise(),
            message: () =>
              'This Number is already assigned to a player. Please use a different one',
          },
        },
      },
      {
        key: 'estimatedValue',
        type: 'input',
        className: 'flex-2',
        templateOptions: {
          label: 'Estimated Value',
          type: 'number',
        },
      },
    ],
  },
];
