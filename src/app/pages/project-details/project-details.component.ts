import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss'],
})
export class ProjectDetailsComponent {
  constructor(private router: Router) {}

  returnToHome() {
    this.router.navigateByUrl('/');
  }
}
