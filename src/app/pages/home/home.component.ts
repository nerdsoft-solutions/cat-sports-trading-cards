import {
  AfterViewInit,
  Component,
  ElementRef,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TradingCardsModel } from '../../models/trading-cards.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TRADING_CARDS_FORM } from '../../forms/trading-cards.form';
import { TradingCardsFacade } from '../../states/trading-cards.facade';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TradingCardComponent } from '../../component/trading-card/trading-card.component';
import { FadeInOutAnimation } from '../../animations/fade-in-out.animation';

@UntilDestroy()
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [FadeInOutAnimation],
})
export class HomeComponent implements AfterViewInit {
  tradingForm: FormGroup = new FormGroup({});
  tradingFormFields: FormlyFieldConfig[] = TRADING_CARDS_FORM(this.facade);
  tradingModel: TradingCardsModel = { estimatedValue: 0 } as TradingCardsModel;

  // @ts-ignore
  @ViewChild('tradingCardsWrapper') cardWrapper: ElementRef;
  // @ts-ignore
  @ViewChildren('cardEl') cardEl: QueryList<TradingCardComponent>;

  constructor(public facade: TradingCardsFacade) {}

  ngAfterViewInit(): void {
    this.cardEl.changes.pipe(untilDestroyed(this)).subscribe(() => {
      this.scrollToBottom();
    });
  }

  addCard(model: TradingCardsModel) {
    this.facade.addStcCard(model);
    this.tradingModel = { estimatedValue: 0 } as TradingCardsModel;
    this.tradingForm.markAsPristine();
  }

  get tradingCardsList(): Observable<TradingCardsModel[]> {
    return this.facade.allTradingCards;
  }

  get totalEstimate(): Observable<number> {
    return this.facade.allTradingCards.pipe(
      map((cardDetails) =>
        cardDetails.reduce((acc, data) => {
          return (acc += data.estimatedValue ? data.estimatedValue : 0);
        }, 0)
      )
    );
  }

  scrollToBottom(): void {
    const scrollContainer = this.cardWrapper?.nativeElement;
    // @ts-ignore
    this.cardWrapper?.nativeElement.scrollTop =
      scrollContainer.scrollHeight - scrollContainer.clientHeight;
  }
}
