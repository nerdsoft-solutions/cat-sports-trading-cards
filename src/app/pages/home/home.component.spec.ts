import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { TradingCardsFacade } from '../../states/trading-cards.facade';
import { CUSTOM_ELEMENTS_SCHEMA, QueryList } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { tradingCardsMock } from '../../mocks/trading-cards.mock';
import { take } from 'rxjs/operators';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import {
  TradingCardsReducer,
  tradingCardsStateKey,
} from '../../states/trading-cards.reducer';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TradingCardComponent } from '../../component/trading-card/trading-card.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  let facade: TradingCardsFacade;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [HomeComponent],
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({
          [tradingCardsStateKey]: TradingCardsReducer,
        }),
      ],
      providers: [TradingCardsFacade],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    facade = TestBed.inject(TradingCardsFacade);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return total value', () => {
    jest
      .spyOn(facade, 'allTradingCards', 'get')
      .mockReturnValue(of(tradingCardsMock));

    component.totalEstimate.pipe(take(1)).subscribe((value) => {
      expect(value).toBe(31);
    });
  });

  it('should return total value if it has 0 value', () => {
    const mock = [
      ...tradingCardsMock,
      {
        playerNumber: 21,
        name: {
          firstName: 'FirstName',
          lastName: 'LastName',
        },
        teamName: 'TEAM',
        estimatedValue: 0,
      },
    ];
    jest.spyOn(facade, 'allTradingCards', 'get').mockReturnValue(of(mock));

    component.totalEstimate.pipe(take(1)).subscribe((value) => {
      expect(value).toBe(31);
    });
  });

  it('should return trading cards list from facade', () => {
    const spy = jest.spyOn(facade, 'allTradingCards', 'get');
    component.tradingCardsList.pipe(take(1)).subscribe();
    expect(spy).toHaveBeenCalled();
  });

  it('should add card when called', () => {
    const spy = jest.spyOn(facade, 'addStcCard');
    component.addCard({
      playerNumber: 21,
      name: {
        firstName: 'FirstName',
        lastName: 'LastName',
      },
      teamName: 'TEAM',
      estimatedValue: 10,
    });
    expect(spy).toHaveBeenCalledWith({
      playerNumber: 21,
      name: {
        firstName: 'FirstName',
        lastName: 'LastName',
      },
      teamName: 'TEAM',
      estimatedValue: 10,
    });
  });

  it('should call scrollToBottom when a new card is added', () => {
    const spy = jest.spyOn(component, 'scrollToBottom');
    component.addCard({
      playerNumber: 21,
      name: {
        firstName: 'FirstName',
        lastName: 'LastName',
      },
      teamName: 'TEAM',
      estimatedValue: 10,
    });
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  });

  it('should scroll to bottom', () => {
    component.cardWrapper = {
      nativeElement: {
        scrollTop: 0,
        scrollHeight: 100,
        clientHeight: 0,
      },
    };
    component.scrollToBottom();
    expect(component.cardWrapper.nativeElement.scrollTop).toBe(100);
  });

  it('should scroll to bottom when card wrapper is empty', () => {
    // @ts-ignore
    component.cardWrapper = undefined;
    component.scrollToBottom();
    //@ts-ignore
    expect(component.cardWrapper?.nativeElement.scrollTop).toBeUndefined();
  });
});
