import { NgModule } from '@angular/core';
import { MAT_COMPONENTS_MODULES } from './mat-components.config';

/**
 * This module is a unified module to load all the needed Angular Material Modules and Components.
 * */
@NgModule({
  imports: MAT_COMPONENTS_MODULES,
  exports: MAT_COMPONENTS_MODULES,
})
export class MatComponentsModule {}
