# CatSportsTradingCards

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.14.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Jest](https://jestjs.io/docs/getting-started).

## Development server
### Local
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Docker
create a .env file in the project root as show below
`CLIENT_IMAGE_NAME=test-trading-cards
TAG=1.0`
Then in command line type `docker-compose up`


[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=arun_sashi_cat-sports-trading-cards%22)
