module.exports = {
  moduleNameMapper: {
    '@core/(.*)': '<rootDir>/src/app/core/$1',
  },
  preset: 'jest-preset-angular',
  testMatch: ["<rootDir>/src/**/*.spec.ts"],
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  collectCoverage: true,
  collectCoverageFrom:[
    '**/*.{ts,tsx}',
    '!**/main.{ts,tsx}',
    '!**/polyfills.{ts,tsx}',
    '!**/index.{ts,tsx}',
    '!**/node_modules/**',
    '!**/vendor/**',
    '!**/dist/**',
    '!**/operators/**.ts',
    '!**/animations/**.ts',
    '!**/environment{.prod,}.ts',
    '!setup-jest.ts',
    '!**/*.mock.ts',
    '!**/public-api.ts',
    '!**/*.model.ts',
    '!**/*.constants.ts',
    '!**/*.enum.{ts,tsx}',
    '!**/*.type.{ts,tsx}',
    '!**/**.config.ts',
    '!**/*.entity.ts',
    '!**/*.{form,fields}.ts',
    '!**/*.module.ts',
    '!**/testing/**.{ts,tsx}'
  ],
  coverageReporters: ['json', 'html', 'text', 'lcovonly'],
  testResultsProcessor: "jest-sonar-reporter",
  coverageThreshold:{
    global:{
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90
    }
  }
};
